/****************************************************************************
*                                                                           *
*   MessagesSplitter plugin for Kadu                                        *
*   Copyright (C) 2011-2014  Piotr Dąbrowski ultr@ultr.pl                   *
*                                                                           *
*   This program is free software: you can redistribute it and/or modify    *
*   it under the terms of the GNU General Public License as published by    *
*   the Free Software Foundation, either version 3 of the License, or       *
*   (at your option) any later version.                                     *
*                                                                           *
*   This program is distributed in the hope that it will be useful,         *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
*   GNU General Public License for more details.                            *
*                                                                           *
*   You should have received a copy of the GNU General Public License       *
*   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
*                                                                           *
****************************************************************************/


#ifndef MESSAGESSPLITTER_H
	#define MESSAGESSPLITTER_H


#include <QObject>

#include "gui/widgets/chat-widget/chat-widget.h"
#include "gui/windows/main-configuration-window.h"
#include "configuration/configuration-aware-object.h"
#include "plugin/plugin-root-component.h"


class MessagesSplitter : public ConfigurationUiHandler, public ConfigurationAwareObject, public PluginRootComponent
{
	Q_OBJECT
	Q_INTERFACES( PluginRootComponent )
	Q_PLUGIN_METADATA( IID "im.kadu.PluginRootComponent" )
	public:
		virtual bool init( bool firstLoad );
		virtual void done();
		MessagesSplitter();
		~MessagesSplitter();
		virtual void mainConfigurationWindowCreated( MainConfigurationWindow *mainConfigurationWindow );
	protected:
		void configurationUpdated();
	private slots:
		void chatWidgetAdded( ChatWidget *chatwidget );
		void chatWidgetRemoved( ChatWidget *chatwidget );
		void messageSendRequested( ChatWidget *chatwidget );
	private:
		void createDefaultConfiguration();
		void wait( int milliseconds );
		bool partssendinglock;
};


#endif
