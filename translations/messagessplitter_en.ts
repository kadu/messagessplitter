<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Messages</translation>
    </message>
    <message>
        <source>Automatic messages splitting</source>
        <translation>Automatic messages splitting</translation>
    </message>
    <message>
        <source>Delay between message parts</source>
        <translation>Delay between message parts</translation>
    </message>
    <message numerus="yes">
        <source>%n millisecond(s)</source>
        <translation>
            <numerusform>%n millisecond</numerusform>
            <numerusform>%n milliseconds</numerusform>
        </translation>
    </message>
    <message>
        <source>Smart messages splitting</source>
        <translation>Smart messages splitting</translation>
    </message>
    <message>
        <source>Remove whitespaces at the end of lines</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
