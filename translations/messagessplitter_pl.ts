<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="pl">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Rozmowa</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Wiadomości</translation>
    </message>
    <message>
        <source>Smart messages splitting</source>
        <translation>Inteligentne dzielenie wiadomości</translation>
    </message>
    <message>
        <source>Automatic messages splitting</source>
        <translation>Automatyczne dzielenie wiadomości</translation>
    </message>
    <message>
        <source>Delay between message parts</source>
        <translation>Opóźnienie pomiędzy częściami wiadomości</translation>
    </message>
    <message numerus="yes">
        <source>%n millisecond(s)</source>
        <translation>
            <numerusform>%n milisekunda</numerusform>
            <numerusform>%n milisekundy</numerusform>
            <numerusform>%n milisekund</numerusform>
        </translation>
    </message>
    <message>
        <source>Remove whitespaces at the end of lines</source>
        <translation>Usuwaj białe znaki na końcu linii</translation>
    </message>
</context>
</TS>
