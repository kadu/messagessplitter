<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs">
<context>
    <name>@default</name>
    <message>
        <source>Chat</source>
        <translation>Rozhovor</translation>
    </message>
    <message>
        <source>Messages</source>
        <translation>Zprávy</translation>
    </message>
    <message>
        <source>Automatic messages splitting</source>
        <translation>Automatické rozdělení zpráv</translation>
    </message>
    <message>
        <source>Delay between message parts</source>
        <translation>Zpoždění mezi částmi zpráv</translation>
    </message>
    <message numerus="yes">
        <source>%n millisecond(s)</source>
        <translation>
            <numerusform>%n milisekunda</numerusform>
            <numerusform>%n milisekundy</numerusform>
            <numerusform>%n milisekund</numerusform>
        </translation>
    </message>
    <message>
        <source>Smart messages splitting</source>
        <translation>Chytré rozdělení zpráv</translation>
    </message>
    <message>
        <source>Remove whitespaces at the end of lines</source>
        <translation></translation>
    </message>
</context>
</TS>
